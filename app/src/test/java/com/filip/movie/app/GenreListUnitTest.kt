package com.filip.movie.app

import org.junit.Test

import org.junit.Assert.*

/**
 * GenreList local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class GenreListUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}
