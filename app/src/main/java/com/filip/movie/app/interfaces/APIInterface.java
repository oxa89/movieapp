package com.filip.movie.app.interfaces;

import com.filip.movie.app.models.GenreList;
import com.filip.movie.app.models.MovieCredits;
import com.filip.movie.app.models.MovieList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("top_rated?api_key=c22d755514350d9836b3f9b173b3d763&language=en-US&page=?")
    Call<MovieList> doGetTopRatedList(@Query("page") String page);

    @GET("popular?api_key=c22d755514350d9836b3f9b173b3d763&language=en-US&page=?")
    Call<MovieList> doGetPopularList(@Query("page") String page);

    @GET("https://api.themoviedb.org/3/genre/movie/list?api_key=c22d755514350d9836b3f9b173b3d763&language=en-US")
    Call<GenreList> doGetAllGenres();

    @GET("https://api.themoviedb.org/3/movie/??api_key=c22d755514350d9836b3f9b173b3d763&append_to_response=credits")
    Call<MovieCredits> doGetMovieCredits();
}
