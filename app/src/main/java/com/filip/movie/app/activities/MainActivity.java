package com.filip.movie.app.activities;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.LinearLayout;
import com.filip.movie.app.APIClient;
import com.filip.movie.app.MovieDetails;
import com.filip.movie.app.R;
import com.filip.movie.app.adapters.ShortListMovieAdapter;
import com.filip.movie.app.interfaces.APIInterface;
import com.filip.movie.app.models.GenreList;
import com.filip.movie.app.models.MovieList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity_TEST";

    private APIInterface apiInterface;

    private MovieList topMovies = new MovieList();
    private MovieList popularMovies = new MovieList();

    RecyclerView topMoviesRecycler, popularMoviesRecycler;

    ShortListMovieAdapter popularListMovieAdapter, topListMovieAdapter;

    MovieDetails movieDetails;

    LinearLayout rootView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rootView = findViewById(R.id.holder_view);

        apiInterface = APIClient.getClient().create(APIInterface.class);

        movieDetails = MovieDetails.getInstance();

        GetMovies getMovies = new GetMovies();
        getMovies.execute();

        topMoviesRecycler = findViewById(R.id.top_rated_recyclerview);
        topMoviesRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        topListMovieAdapter = new ShortListMovieAdapter(this, topMovies.getResults());
        topMoviesRecycler.setAdapter(topListMovieAdapter);

        popularMoviesRecycler = findViewById(R.id.popular_recyclerview);
        popularMoviesRecycler.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        popularListMovieAdapter = new ShortListMovieAdapter(this, popularMovies.getResults());
        popularMoviesRecycler.setAdapter(popularListMovieAdapter);

    }

    private void setPopularMovies() {
        popularListMovieAdapter.setTopMovies(popularMovies.getResults());
    }

    private void setTopMovies() {
        topListMovieAdapter.setTopMovies(topMovies.getResults());
    }

    private class GetMovies extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Log.d(TAG, "onPostExecute: ");

        }

        @Override
        protected Void doInBackground(Void... voids) {
            Call<MovieList> topRatedCall = apiInterface.doGetTopRatedList("1");
            topRatedCall.enqueue(new Callback<MovieList>() {
                @Override
                public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                    topMovies = response.body();
                    Log.d(TAG, "topRatedCall: " + topMovies.getResults().size());
                    setTopMovies();
                }

                @Override
                public void onFailure(Call<MovieList> call, Throwable t) {
                    call.cancel();
                }
            });

            Call<GenreList> genreListCall = apiInterface.doGetAllGenres();
            genreListCall.enqueue(new Callback<GenreList>() {
                @Override
                public void onResponse(Call<GenreList> call, Response<GenreList> response) {
                    movieDetails.setGenres(response.body());
                }

                @Override
                public void onFailure(Call<GenreList> call, Throwable t) {
                    call.cancel();
                }
            });

            Call<MovieList> popularCall = apiInterface.doGetPopularList("1");
            popularCall.enqueue(new Callback<MovieList>() {
                @Override
                public void onResponse(Call<MovieList> call, Response<MovieList> response) {
                    popularMovies = response.body();
                    Log.d(TAG, "popularCall: " + popularMovies.getResults().size());
                    setPopularMovies();
                }

                @Override
                public void onFailure(Call<MovieList> call, Throwable t) {
                    call.cancel();
                }
            });


            return null;
        }
    }

}
