package com.filip.movie.app.interfaces;

public interface MovieClickInterface {
    void onMovieClick(int movieID);
}
