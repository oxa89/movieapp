package com.filip.movie.app.models;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.filip.movie.app.R;
import com.filip.movie.app.adapters.ShortListMovieAdapter;

import java.util.List;

public class MovieListMostPopular extends RecyclerView.ViewHolder {

    private RecyclerView mPopularRecyclerView;
    private ShortListMovieAdapter shortListMovieAdapter;

    public MovieListMostPopular(@NonNull View itemView, List<Movie> movieList) {
        super(itemView);

        mPopularRecyclerView = itemView.findViewById(R.id.popular_recyclerview);
        shortListMovieAdapter = new ShortListMovieAdapter(itemView.getContext(), movieList);
        mPopularRecyclerView.setAdapter(shortListMovieAdapter);
        mPopularRecyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
    }
}
