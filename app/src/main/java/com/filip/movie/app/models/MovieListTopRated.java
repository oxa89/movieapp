package com.filip.movie.app.models;

import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import com.filip.movie.app.R;
import com.filip.movie.app.adapters.ShortListMovieAdapter;

import java.util.List;

public class MovieListTopRated extends RecyclerView.ViewHolder {

    private RecyclerView mTopRatedRecyclerView;
    private ShortListMovieAdapter shortListMovieAdapter;

    public MovieListTopRated(@NonNull View itemView, List<Movie> movieList) {
        super(itemView);

        mTopRatedRecyclerView = itemView.findViewById(R.id.top_rated_recyclerview);
        shortListMovieAdapter = new ShortListMovieAdapter(itemView.getContext(), movieList);
        mTopRatedRecyclerView.setAdapter(shortListMovieAdapter);
        mTopRatedRecyclerView.setLayoutManager(new LinearLayoutManager(itemView.getContext(), LinearLayoutManager.HORIZONTAL, false));
    }
}