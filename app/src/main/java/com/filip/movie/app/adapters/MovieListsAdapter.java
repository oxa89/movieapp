package com.filip.movie.app.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.filip.movie.app.R;
import com.filip.movie.app.models.MovieList;
import com.filip.movie.app.models.MovieListMostPopular;
import com.filip.movie.app.models.MovieListTopRated;

public class MovieListsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_TYPE_TOP_RATED = 0;
    private static final int ITEM_TYPE_MOST_POPULAR = 1;

    private MovieList topMovies;
    private MovieList popularMovies;

    public MovieListsAdapter(MovieList topMovies, MovieList popularMovies ) {
        this.topMovies = topMovies;
        this.popularMovies = popularMovies;
        Log.d("MainActivity_TEST", "MovieListsAdapter");
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return ITEM_TYPE_TOP_RATED;
            case 1:
                return ITEM_TYPE_MOST_POPULAR;
            default:
                return ITEM_TYPE_TOP_RATED;
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        switch (getItemViewType(position)) {
            case ITEM_TYPE_TOP_RATED:
                return new MovieListTopRated(inflater.inflate(R.layout.item_movies_top_rated_list, viewGroup, false), topMovies.getResults());
            case ITEM_TYPE_MOST_POPULAR:
                return new MovieListMostPopular(inflater.inflate(R.layout.item_movies_most_popular, viewGroup, false), popularMovies.getResults());
            default:
                return new MovieListTopRated(inflater.inflate(R.layout.item_movies_top_rated_list, viewGroup, false), topMovies.getResults());
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return 2;
    }

}
