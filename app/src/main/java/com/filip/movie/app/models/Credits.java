package com.filip.movie.app.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Credits {

    @SerializedName("cast")
    @Expose
    private List<Cast> cast = null;

    /**
     * No args constructor for use in serialization
     *
     */
    public Credits() {
    }

    /**
     *
     * @param cast
     */
    public Credits(List<Cast> cast) {
        super();
        this.cast = cast;
    }

    public List<Cast> getCast() {
        return cast;
    }

    public void setCast(List<Cast> cast) {
        this.cast = cast;
    }

    public Credits withCast(List<Cast> cast) {
        this.cast = cast;
        return this;
    }

}