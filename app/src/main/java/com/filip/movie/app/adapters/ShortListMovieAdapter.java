package com.filip.movie.app.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.filip.movie.app.MovieDetails;
import com.filip.movie.app.R;
import com.filip.movie.app.activities.MovieDetailsActivity;
import com.filip.movie.app.models.Movie;
import com.filip.movie.app.moduls.GlideApp;

import java.util.List;

public class ShortListMovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Movie> topMovies;
    private Context context;

    MovieDetails movieDetails;

    public ShortListMovieAdapter(Context context, List<Movie> topMovies) {
        this.context = context;
        this.topMovies = topMovies;
        movieDetails = MovieDetails.getInstance();
    }

    public void setTopMovies(List<Movie> topMovies) {
        this.topMovies = topMovies;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        return new MovieDetailSmall(inflater.inflate(R.layout.item_movie_small, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        MovieDetailSmall movieDetailSmall = (MovieDetailSmall) viewHolder;
        movieDetailSmall.movieTitle.setText(topMovies.get(position).getTitle());
        movieDetailSmall.movieGenre.setText(getGenres(position));
        movieDetailSmall.movieRating.setText(String.format(context.getString(R.string.rating_text), topMovies.get(position).getVoteAverage()));
        GlideApp.with(context)
                .asDrawable()
                .load("http://image.tmdb.org/t/p/w500" + topMovies.get(position).getPosterPath())
                .override(300, 450)
                .centerCrop() // scale to fill the ImageView and crop any extra
                .into(movieDetailSmall.movieImage);
    }

    @Override
    public int getItemCount() {
        return topMovies.size();
    }

    private String getGenres(int position) {
        String genres = "";
        for (int i : topMovies.get(position).getGenreIds()) {
            genres = genres.concat(movieDetails.getGenreWithId(i) + ", ");
        }
        if (genres.length() > 2)
            return genres.substring(0, genres.length() - 2);
        else
            return genres;
    }

    class MovieDetailSmall extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView movieImage;
        TextView movieTitle, movieGenre, movieRating;

        public MovieDetailSmall(@NonNull View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);

            movieImage = itemView.findViewById(R.id.movie_image);
            movieTitle = itemView.findViewById(R.id.movie_title);
            movieGenre = itemView.findViewById(R.id.movie_genre);
            movieRating = itemView.findViewById(R.id.movie_rating);
        }

        @Override
        public void onClick(View view) {
            Intent movieIntent = new Intent(view.getContext(), MovieDetailsActivity.class);
            movieIntent.putExtra("MOVIE_ID", topMovies.get(getAdapterPosition()).getId());
            view.getContext().startActivity(movieIntent);
        }
    }
}
