package com.filip.movie.app.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.TextView
import com.filip.movie.app.APIClient
import com.filip.movie.app.R
import com.filip.movie.app.interfaces.APIInterface
import com.filip.movie.app.models.Genre

class MovieDetailsActivity : AppCompatActivity() {

    lateinit var image : ImageView
    lateinit var title: TextView
    lateinit var genre: TextView
    lateinit var rating: TextView
    lateinit var description: TextView
    lateinit var crew: RecyclerView
    lateinit var recommendation: RecyclerView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_details)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)

        image = findViewById(R.id.movie_image)
        title = findViewById(R.id.movie_title)
        genre = findViewById(R.id.movie_genre)
        rating = findViewById(R.id.movie_rating)
        description = findViewById(R.id.movie_description)

        var apiInterface = APIClient.getClient().create(APIInterface::class.java)

    }

    private fun getGenres( genres: List<Genre>): String {
        var genresString = ""
        for (i in genres) {
            genresString += ("${i.name}, ")
        }
        return if (genresString.length > 2)
            genresString.substring(0, genresString.length - 2)
        else
            genresString
    }
}
