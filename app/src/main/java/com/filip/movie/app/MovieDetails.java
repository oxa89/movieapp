package com.filip.movie.app;

import com.filip.movie.app.models.Genre;
import com.filip.movie.app.models.GenreList;

public class MovieDetails {
    private static final MovieDetails ourInstance = new MovieDetails();

    private GenreList genres;

    public static MovieDetails getInstance() {
        return ourInstance;
    }

    public GenreList getGenres() {
        return genres;
    }

    public void setGenres(GenreList genres) {
        this.genres = genres;
    }

    public String getGenreWithId(int id) {
        if (genres != null)
            for (Genre g : genres.getGenres()) {
                if (g.getId() == id)
                    return g.getName();
            }
        return "";
    }
}
